﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DocVelocity.Integration.Helpers.Logging;
using NLog.Config;
using NLog.Targets;
using NLog.Targets.Wrappers;
using DataLayer;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var loggerfactory = new NLogLoggerFactory();
            var logger = loggerfactory.GetLogger("Console");

            var loggerHP = loggerfactory.GetLogger("HomePoint");
            var connectionHP= "server=127.0.0.1;port=3306;database=eventdbhp;uid=root;password=docV7491!";
            var eventGrabberHP = new EventStatGrabber(
                connectionHP,
                "stonegatemtg.capsilon.net",
                "Integration-Framework",
                "Homepoint1!",
                "stonegatemtgtst.loankatalyst3.net.p12",
                "$t0n3G@t3@2017",
                loggerHP);

            var eventThrowerHP = new EventThrower(
                connectionHP,
                "stonegatemtg.capsilon.net",
                "Integration-Framework",
                "Homepoint1!",
                "stonegatemtgtst.loankatalyst3.net.p12",
                "$t0n3G@t3@2017",
                loggerHP);
                 
          var folderGrabberHP = new FolderStatGrabber(
              connectionHP,
              "stonegatemtg.capsilon.net",
              "Integration-Framework",
              "Homepoint1!",
              "stonegatemtgtst.loankatalyst3.net.p12",
              "$t0n3G@t3@2017",
              loggerHP);

          var loggerPRMG = loggerfactory.GetLogger("PRMG");
          var connectionPRMG = "server=127.0.0.1;port=3306;database=eventdbprmg;uid=root;password=docV7491!";
          var eventGrabberPRMG = new EventStatGrabber(
              connectionPRMG,
              "prmg.docvelocity-na10.net",
              "integration-framework",
              "C@psi1on",
              "prmg.docvelocity-na10.net.p12",
              "Prmg@DVN@10@20!7",
              loggerPRMG);

          var folderGrabberPRMG = new FolderStatGrabber(
              connectionPRMG,
              "prmg.docvelocity-na10.net",
              "integration-framework",
              "C@psi1on",
              "prmg.docvelocity-na10.net.p12",
              "Prmg@DVN@10@20!7",
              loggerPRMG);

            var loggerLSP = loggerfactory.GetLogger("LSP");
            var connectionLSP = "server=127.0.0.1;port=3306;database=eventdblsp;uid=root;password=docV7491!";
            var eventGrabberLSP = new EventStatGrabber(
                connectionLSP,
                "lsp.docvelocity.net",
                "integration-framework",
                "7=Ha5k`Kmxu`6dTnD%",
                "lsp.docvelocity.net.p12",
                "L$p@D0cv@20!7",
                loggerLSP);

            var folderGrabberLSP = new FolderStatGrabber(
                connectionLSP,
                "lsp.docvelocity.net",
                "integration-framework",
                "7=Ha5k`Kmxu`6dTnD%",
                "lsp.docvelocity.net.p12",
                "L$p@D0cv@20!7",
                loggerLSP);


            Parallel.ForEach(
                new List<(EventStatGrabber, FolderStatGrabber)> { (eventGrabberPRMG, folderGrabberPRMG), (eventGrabberHP, folderGrabberHP), (eventGrabberLSP, folderGrabberLSP) },
                x=> {
                    var result = x.Item1.GrabEventsIntoDb();
                    var res = 0;
                    do
                    {
                        res = x.Item2.FillEventFoldersMDIntoDb();
                    } while (res > 0);
                });   
            
        }

    }
}
