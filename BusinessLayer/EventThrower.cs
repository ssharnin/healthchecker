﻿using DataLayer;
using DocVelocity.Integration.Helpers.Logging;
using DocVelocity.Orchestration.SDK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class EventThrower
    {
        public string SiteAddress { get; }
        private ICloud _dvCloud;
        private MySQLEventsPersistenceStore _store;
        private ILogger _logger;

        public EventThrower(string connectionString, string siteAddress, string appName, string credentials, string certFileName, string certSecretPhrase, ILogger logger)
        {
            SiteAddress = siteAddress;
            _logger = logger;
            _dvCloud = new CloudInitialiser(logger).GetDvCloud(siteAddress, appName, credentials, certFileName, certSecretPhrase);
            _store = new MySQLEventsPersistenceStore(connectionString, logger);
        }

        public void DeleteEvents(int step)
        {
            var events = _store.GetEventsWithoutMDId(0, 80000);
            var folders = events.Select(x=>x.SearchContextId).Distinct().OrderBy(x=>x).Skip(step*1000).Take(1000).ToList();
            _logger.Info($"DONE {events.Count} events were Selected'");
            _logger.Info($"DONE {folders.Count} folders were Found'");
            var sw = Stopwatch.StartNew();
            Parallel.ForEach(folders, new ParallelOptions { MaxDegreeOfParallelism = 20 }, x => DeleteEvents(events.Where(e=>e.SearchContextId == x).ToList(), x));
            sw.Stop();
            _logger.Info($"DONE {events.Count} events were Acknowledged in {sw.ElapsedMilliseconds}");
        }

        private void DeleteEvents(List<Event> events, string folderId)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                var start = 0;
                var eventIds = events.Select(x => x.Id).ToList();
                var stepevents = eventIds.Skip(start).Take(100).ToList();
                if (events.Count > 100)
                {
                    while (stepevents.Count > 0)
                    {
                        start = start + 100;

                        _dvCloud.PlatformEvents.AcknowledgeMany(stepevents);
                        stepevents = eventIds.Skip(start).Take(100).ToList();
                    } ;
                }
                else
                {
                    _dvCloud.PlatformEvents.AcknowledgeMany(eventIds);
                }
               
                sw.Stop();
                _logger.Info($"--{events.Count} events Acknowledged for {folderId} in {sw.ElapsedMilliseconds}");
                sw.Restart();
                _store.AddFolderInfo(new Folder { Id = folderId, MetadataId = "Acknowledged" });
                _logger.Info($"--{events.Count} events STatus were changed for {folderId} in {sw.ElapsedMilliseconds}");

            }
            catch (Exception ex)
            {
                _store.AddFolderInfo(new Folder { Id = folderId, MetadataId = "Skipped" });
                _logger.Error(ex);
            }

        }
    }
}
