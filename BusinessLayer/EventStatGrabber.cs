﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataLayer;
using DocVelocity.Integration.Helpers.Logging;
using DocVelocity.Orchestration.SDK;
using DocVelocity.Orchestration.SDK.Entities.PlatformEvents;

namespace BusinessLayer
{
    public class EventStatGrabber
    {
        public string SiteAddress { get; }
        private ICloud _dvCloud;
        private MySQLEventsPersistenceStore _store;
        private ILogger _logger;

        public EventStatGrabber(string connectionString, string siteAddress, string appName, string credentials, string certFileName, string certSecretPhrase, ILogger logger)
        {
            SiteAddress = siteAddress;
            _logger = logger;
            _dvCloud = new CloudInitialiser(logger).GetDvCloud(siteAddress, appName, credentials, certFileName, certSecretPhrase);
            _store = new MySQLEventsPersistenceStore(connectionString, logger);
        }
        public int GrabEventsIntoDb()
        {
            var events = GetPlatformEventsTest(_dvCloud);
            _logger.Info($"Grabbed {events.Count} events ");
            return events.Count;
        }

        private IList<PlatformEvent> GetPlatformEventsTest(ICloud cloud)
        {
            List<PlatformEvent> dvEvents = new List<PlatformEvent>();
            List<string> dvEventIds = new List<string>();
            try
            {
                int start = 0;
                int step = 100;
                int eventsinQ = 0;
                while (eventsinQ > 0 || start == 0)
                {
                    var events = cloud.PlatformEvents.GetNextCollectionByCriteria(
                        start < 0 ? 0 : start, 
                        step, 
                        "",
                        new List<Expression<Func<PlatformEvent, object>>> { 
                            @event => @event.Id,
                            @event => @event.Type,
                            @event => @event.EventGenerationTime,
                            @event => @event.SearchContextId,
                            @event => @event.EventOriginatorId,
                            @event => @event.EventOriginatorName,
                            @event => @event.EventOriginatorSessionType,
                            @event => @event.EventDetail
                        }).ToList();

                    start = start + step;
                    eventsinQ = events.Count;

                    if (eventsinQ > 0)
                    {
                        var distinctEvents = events.Where(x => !dvEventIds.Contains(x.Id));
                        _store.AddEventInfoRange(distinctEvents.Select(ev => new Event(ev)).ToList());
                        _logger.Info($"- {distinctEvents.Count()} were added to DB");
                        dvEvents.AddRange(distinctEvents);
                        dvEventIds.AddRange(events.Select(x=>x.Id));
                    }
                }
            }
            catch (Exception ex )
            {
                _logger.Error(ex);
            }

            return dvEvents;
        }

        public int GrabEventsCount()
        {
            return GetPlatformEventsCount(_dvCloud);
        }

        private int GetPlatformEventsCount(ICloud cloud)
        {
            int eventsinQ = 0;
            try
            {
                int start = 490;
                int end = 510;
                int step = 1000;      
                var dict = new Dictionary<int,List<PlatformEvent>>();
                var calls = Enumerable.Range(480, end - start).ToList();

                calls.AsParallel().ForAll(x => GEtEvents(cloud, x, step, dict));               
                

                eventsinQ = dict.Keys.Max();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return eventsinQ;
        }

        private void GEtEvents(ICloud cloud, int x, int step, Dictionary<int, List<PlatformEvent>> dict)
        {
            var events = cloud.PlatformEvents.GetNextCollectionByCriteria(
                                    x * step,
                                    100,
                                    "",
                                    new List<Expression<Func<PlatformEvent, object>>> {
                            @event => @event.Id,
                            @event => @event.Type,
                            @event => @event.EventGenerationTime,
                            @event => @event.EventOriginatorName
                                    }).ToList();

            _logger.Info($"Received {events.Count} events from {x * step} ");

            if (events.Count > 0)
                dict.Add(x * step, events);
        }

        public void GrabEventsIntoDbFast()
        {
            GetPlatformEventsFast(_dvCloud);
        }
        private void GetPlatformEventsFast(ICloud cloud)
        {
            int eventsinQ = 10000;
            
            try
            {
                int step = 100;
                int start = 0;
                int end = eventsinQ/ step;  
                var dict = new Dictionary<int, List<PlatformEvent>>();

                var calls = Enumerable.Range(start, end - start).ToList();
                calls.AsParallel().ForAll(x => GEtEvents(cloud, x, step, dict));


                eventsinQ = dict.Keys.Max();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }      
            
            try
            {
                int start = 0;
                int end = eventsinQ/100;

                var calls = Enumerable.Range(start, end+10).ToList();

                Parallel.For(start, end + 10, new ParallelOptions { MaxDegreeOfParallelism = 30}, x => GrabEvents(cloud, x, 100));              
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }        
        }

        private void GrabEvents(ICloud cloud, int x, int step)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                var events = cloud.PlatformEvents.GetNextCollectionByCriteria(
                                        x * step,
                                        100,
                                        "",
                                        new List<Expression<Func<PlatformEvent, object>>> {
                               @event => @event.Id,
                            @event => @event.Type,
                            @event => @event.EventGenerationTime,
                            @event => @event.SearchContextId,
                            @event => @event.EventOriginatorId
                                        }).ToList();
                sw.Stop();
                _logger.Info($"Received {events.Count} events from {x * step} in {sw.ElapsedMilliseconds}");

                if (events.Count > 0)
                {
                    _store.AddEventInfoRange(events.Select(ev => new Event(ev)).ToList());
                    _logger.Info($"- {events.Count()} were added to DB");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

    }
}
