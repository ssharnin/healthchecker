﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using DataLayer;
using DocVelocity.Integration.Helpers.Logging;
using DocVelocity.Integration.Helpers.Logging.Metrics;
using DocVelocity.Orchestration.SDK;
using DocVelocity.Orchestration.SDK.Entities;
using DocVelocity.Orchestration.SDK.Entities.PlatformEvents;
using Folder = DataLayer.Folder;

namespace BusinessLayer
{
    public class FolderStatGrabber
    {
        public string SiteAddress { get; }
        private ICloud _dvCloud;
        private MySQLEventsPersistenceStore _store;
        private ILogger _logger;

        public FolderStatGrabber(string connectionString, string siteAddress, string appName, string credentials, string certFileName, string certSecretPhrase, ILogger logger)
        {
            SiteAddress = siteAddress;
            _logger = logger;
            _dvCloud = new CloudInitialiser(logger).GetDvCloud(siteAddress, appName, credentials, certFileName, certSecretPhrase);
            _store = new MySQLEventsPersistenceStore(connectionString, logger);
        }
        public int FillEventFoldersMDIntoDb()
        {
            var folders = GetFoldersWithoutMDId();
            return _logger.TraceWithMetrics(LogEntry.New("GetMdForFolderAndStoreIt"), _ => GetMdForEventsAndStore(folders), "");
        }
        private IList<Folder> GetFoldersWithoutMDId()
        {
            try
            {
                return _store.GetFoldersWithoutMDId();
            }
            catch (Exception e)
            {
                _logger.Error(LogEntry.New(
                        $"Error of getting folders without md")
                    .WithException(e));
            }
            return new List<Folder>();
        }

        private int GetMdForEventsAndStore(IList<Folder> folders)
        {
            return folders.AsParallel().Select(GetMdForFolderAndStoreIt).Count(x=>x);
        }

        private bool GetMdForFolderAndStoreIt( Folder folder1)
        {            
            try
            {
                var sw = Stopwatch.StartNew();
                var md = GetMD(folder1);
                sw.Stop();
                if (md != null)
                {
                    folder1.MetadataId = md.MetaValue + " " + md.LastUpdatedBy;
                    folder1.MirrorDate = md.LastUpdatedTime?.ToString("O");
                    _logger.Info($"Folder {folder1.Id} is mirrored. {sw.ElapsedMilliseconds} ms");
                }
                else
                {
                    _logger.Info($"Folder {folder1.Id} is not mirrored. {sw.ElapsedMilliseconds} ms");
                }
                
                if (_store.AddFolderInfo(folder1))
                    return true;
            }
            catch (Exception e)
            {
                _logger.Error(LogEntry.New(
                        $"[{folder1.Id}] Error of getting folder md",
                        folder1)
                    .WithException(e)
                    .WithCorrelationId(folder1.Id));
            }

            return false;
        }

        public Metadata GetMD(Folder folder1)
        {
            var mds = _dvCloud.Metadata.SearchMetadata(
                0,
                20,
                objectRef: folder1.Id,
                searchContextId: folder1.Id,
                objectType: ObjectType.FOLDER
            );

            return mds.FirstOrDefault(x => x.MetaKey == "LOSLOANID");
        }

        public List<Metadata> GetMD(string loanGuid)
        {
            var mds = _dvCloud.Metadata.SearchMetadata(0,20,
                metaKey: "LOSLOANID",
                metaValue: loanGuid
            ).ToList();

            return mds;
        }

    }
}
