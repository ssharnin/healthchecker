﻿using System;
using DocVelocity.Integration.Helpers.Logging;
using DocVelocity.Orchestration.SDK;
using DocVelocity.Orchestration.SDK.Certificates;
using DocVelocity.Orchestration.SDK.Entities;
using DocVelocity.Orchestration.SDK.Entities.Enums;
using DocVelocity.Orchestration.SDK.Settings;
using DocVelocity.Orchestration.SDK.Tickets;

namespace BusinessLayer
{
    internal class CloudInitialiser
    {
        private ILogger _logger;

        public CloudInitialiser(ILogger logger)
        {
            _logger = logger;
        }

        internal ICloud GetDvCloud(string siteAddress, string appName, string credentials, string certFileName, string certSecretPhrase)
        {
            
            ConnectionSettings conn = new ConnectionSettings()
            {
                SiteAddress = siteAddress,
                AppName = appName,
                UseSsl = true
            };
            ICertificateResolver cert = new NopCertificateResolver();
            if (certFileName != null && certSecretPhrase != null)
            {
                cert = new FileCertificateResolver(_logger)
                {
                    FileName = certFileName,
                    Password = certSecretPhrase
                };
            }

            return new Cloud(new CloudSettings
            {
                CertificateResolver = cert,
                Connection = conn,
                DefaultTicketManager = new TicketManagerSettings
                {
                    SessionType = SessionType.SiteUser,
                    Credentials = credentials,
                    ApplicationTicketCount = 10,
                    UserTicketCount = 10,
                    ReservationTime = TimeSpan.Parse("00:01:00"),
                    SaveDumpDuringVolationException = false,
                    SizeOfHistoryDump = 10,
                    Strategy = StrategyOfOverwhelmingTickets.WaitingTicket,
                    UserDetails = new UserDetails{UserId = "HealthChecker", LastName = "Health", FirstName = "Checker", Email =  "email@email.com"}
                },
                Download = new DownloadSettings
                {
                    RetryTimeout = TimeSpan.FromMinutes(10),
                    RetryInterval = TimeSpan.FromMilliseconds(100)
                },
                Http = new HttpSettings
                {
                    UseHttpClient = true,
                    Client = new HttpClientSettings(),
                },
                Retry = new RetrySettings
                {
                    DelayBetweenRetry = TimeSpan.Parse("00:00:01"),
                    MaxRetryCount = 3
                },
                Logger = _logger
            });
        }
    }
}
