﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DocVelocity.Integration.Helpers.Logging;
using MySql.Data.MySqlClient;

namespace DataLayer
{
    public class MySQLEventsPersistenceStore
    {
        private readonly ILogger _logger;
        private readonly string _connectionString;
        private bool _isTableCreated;

        private const string CREATE_TABLE_EVENTS_SQL =
            "CREATE TABLE IF NOT EXISTS SyncEvents43 (Id varchar(100), MDId varchar(150), MDDate varchar(100), Date Timestamp, Type varchar(50), " +
            "EventOriginatorId varchar(50), EventOriginatorName varchar(50), EventQueueName varchar(15), SearchContextId varchar(50), EventDetail varchar(3000));";
        
        private const string GetFolders_Without_mdId =
            "SELECT distinct  SearchContextId FROM SyncEvents43 WHERE MDId is null  Limit 0,30";

        private const string GetEvents_Without_mdId =
            "SELECT distinct Id, SearchContextId  FROM SyncEvents43 WHERE MDId = ''  Limit @start, @end";

        private const string INSERT_EVENT_SQL =
            "INSERT INTO SyncEvents43 VALUES(@Id, null, null, @Date,@Type,@EventOriginatorId,@EventOriginatorName,@EventQueueName,@SearchContextId,@EventDetail)";

        private const string UPDATE_MDIDFOLDER_EVENT_SQL =
            "UPDATE SyncEvents43 SET MDId = @MDId, MDDate = @MDDate WHERE SearchContextId = @SearchContextId";

        private const string UPDATE_MDIDFOLDER_EVENT_SQL_by_Id =
            "UPDATE SyncEvents43 SET MDId = @MDId WHERE Id = @Id";

        public MySQLEventsPersistenceStore(string connectionString, ILogger logger)
        {
            _connectionString = connectionString;
            _logger = logger;            
        }

        private void InitSyncDatesTable(MySqlConnection connection)
        {
            using (var command = new MySqlCommand(CREATE_TABLE_EVENTS_SQL, connection))
            {
                command.ExecuteNonQuery();
                _logger.Info("SyncDates table was created");
            }
        }

        public int AddEventInfoRange(IList<Event> eventInfos, string correlationId = null)
        {
            try
            {
                int rowAddedCounter = 0;

                using (var connection = new MySqlConnection(_connectionString))
                {
                    connection.Open();

                    if (!_isTableCreated)
                    {
                        InitSyncDatesTable(connection);
                        _isTableCreated = true;
                    }

                    using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        try
                        {
                            using (var cmd = new MySqlCommand(INSERT_EVENT_SQL, connection))
                            {
                                AddEventParameters(cmd);
                                cmd.Transaction = transaction;
                                foreach (var eventInfo in eventInfos)
                                {
                                    InitEventParameters(cmd, eventInfo);
                                    rowAddedCounter += cmd.ExecuteNonQuery();
                                }
                            }
                            transaction.Commit();
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            foreach (var eventInfo in eventInfos)
                            {
                                _logger.Error(LogEntry.New($"[{eventInfo.Id}] Error adding event info collection", eventInfo)
                                    .WithException(e)
                                    .WithCorrelationId(correlationId));
                            }
                        }
                    }
                }

                return rowAddedCounter;
            }
            catch (MySqlException e)
            {
                foreach (var eventInfo in eventInfos)
                {
                    _logger.Error(LogEntry.New(
                            $"[{eventInfo.Id}] Error adding event info collection",
                            eventInfo)
                        .WithException(e)
                        .WithCorrelationId(correlationId));
                }
            }

            return 0;
        }

        public bool AddFolderInfo(Folder folderInfo, string correlationId = null)
        {
             using (var connection = new MySqlConnection(_connectionString))
                {
                    connection.Open();
       

                   
                        try
                        {
                            using (var cmd = new MySqlCommand(UPDATE_MDIDFOLDER_EVENT_SQL, connection))
                            {
                                AddFolderInfoParameters(cmd);
                                InitFolderInfoParameters(cmd, folderInfo);
                                cmd.ExecuteNonQuery();
                                
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error(LogEntry.New(
                                    $"[{folderInfo.Id}] Error adding folder info",
                                    folderInfo)
                                .WithException(e)
                                .WithCorrelationId(correlationId));
                            return false;
                    }
                   
                }

                return true;
        }

        public bool AddMDInfo(string eventId, string mdId, string correlationId = null)
        {
            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();



                try
                {

                    using (var cmd = new MySqlCommand(UPDATE_MDIDFOLDER_EVENT_SQL_by_Id, connection))
                    {
                        var sw = Stopwatch.StartNew();
                        AddEventInfoParameters(cmd);
                        InitEventInfoParameters(cmd, eventId, mdId);
                        cmd.ExecuteNonQuery();
                        sw.Stop();
                        _logger.Info($"-- {eventId} event state was changed to {mdId} in {sw.ElapsedMilliseconds}");
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(LogEntry.New(
                            $"[{eventId}] Error adding MD info",
                            eventId)
                        .WithException(e)
                        .WithCorrelationId(correlationId));
                    return false;
                }

            }

            return true;
        }

        public List<Folder> GetFoldersWithoutMDId(string correlationId = null)
        {
            var ret = new List<Folder>();
            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new MySqlCommand(GetFolders_Without_mdId, connection))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ret.Add(ReadFolderFromEvent(reader));
                        }
                    }
                }
            }
            return ret;
        }

        private static Folder ReadFolderFromEvent(IDataRecord reader) =>
            new Folder
            {
                Id = reader["SearchContextId"].ToString()
            };

        private static void AddEventParameters(MySqlCommand cmd)
        {
            cmd.Parameters.Add("@Id", MySqlDbType.String, 0, "@ID");
            cmd.Parameters.Add("@MDId", MySqlDbType.String, 0, "@MDId");
            cmd.Parameters.Add("@MDDate", MySqlDbType.String, 0, "@MDDate");
            cmd.Parameters.Add("@Date", MySqlDbType.Timestamp, 0, "@Date");
            cmd.Parameters.Add("@Type", MySqlDbType.String, 0, "@Type");
            cmd.Parameters.Add("@EventOriginatorId", MySqlDbType.String, 0, "@EventOriginatorId");
            cmd.Parameters.Add("@EventOriginatorName", MySqlDbType.String, 0, "@EventOriginatorName");
            cmd.Parameters.Add("@EventQueueName", MySqlDbType.String, 0, "@EventQueueName");
            cmd.Parameters.Add("@SearchContextId", MySqlDbType.String, 0, "@SearchContextId");
            cmd.Parameters.Add("@EventDetail", MySqlDbType.String, 0, "@EventDetail");
        }
        private static void InitEventParameters(MySqlCommand cmd, Event eventInfo)
        {
            cmd.Parameters["@Id"].Value = eventInfo.Id;
            cmd.Parameters["@Date"].Value = eventInfo.EventGenerationTime;
            cmd.Parameters["@Type"].Value = eventInfo.Type;
            cmd.Parameters["@EventOriginatorId"].Value = eventInfo.EventOriginatorId;
            cmd.Parameters["@EventOriginatorName"].Value = eventInfo.EventOriginatorName;
            cmd.Parameters["@EventQueueName"].Value = eventInfo.EventQueueName;
            cmd.Parameters["@SearchContextId"].Value = eventInfo.SearchContextId;
            cmd.Parameters["@EventDetail"].Value = (eventInfo.EventDetail?.Length??0) > 3000 ? eventInfo.EventDetail.Substring(0, 3000) : eventInfo.EventDetail;
        }

        private static void AddEventInfoParameters(MySqlCommand cmd)
        {
            cmd.Parameters.Add("@Id", MySqlDbType.String, 0, "@ID");
            cmd.Parameters.Add("@MDId", MySqlDbType.String, 0, "@MDId");
        }
        private static void InitEventInfoParameters(MySqlCommand cmd, string eventId, string mdId)
        {
            cmd.Parameters["@Id"].Value = eventId;
            cmd.Parameters["@MDId"].Value = mdId;
        }

        private static void AddFolderInfoParameters(MySqlCommand cmd)
        {
            cmd.Parameters.Add("@SearchContextId", MySqlDbType.String, 0, "@SearchContextId");
            cmd.Parameters.Add("@MDId", MySqlDbType.String, 0, "@MDId");
            cmd.Parameters.Add("@MDDate", MySqlDbType.String, 0, "@MDDate");
        }
        private static void InitFolderInfoParameters(MySqlCommand cmd, Folder folderInfo)
        {
            cmd.Parameters["@SearchContextId"].Value = folderInfo.Id;
            cmd.Parameters["@MDId"].Value = folderInfo.MetadataId??"";
            cmd.Parameters["@MDDate"].Value = folderInfo.MirrorDate ?? "";
        }

        public List<Event> GetEventsWithoutMDId(int start, int end, string correlationId = null)
        {
            var ret = new List<Event>();
            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new MySqlCommand(GetEvents_Without_mdId, connection))
                {
                    cmd.Parameters.Add("@start", MySqlDbType.Int32, 0, "@start");
                    cmd.Parameters.Add("@end", MySqlDbType.Int32, 0, "@end");

                    cmd.Parameters["@start"].Value = start;
                    cmd.Parameters["@end"].Value = end;

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ret.Add(ReadEvent(reader));
                        }
                    }
                }
            }
            return ret;
        }

        private static Event ReadEvent(IDataRecord reader) =>
       new Event
       {
           Id = reader["Id"].ToString()  ,
           SearchContextId = reader["SearchContextId"].ToString()
       };  

    }
}
