﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocVelocity.Orchestration.SDK.Entities.PlatformEvents;
using DocVelocity.Orchestration.SDK.Entities.PlatformEvents.EventDetails;

namespace DataLayer
{
    public class Event
    {
        public string Id { get; set; }
        public string MdId { get; set; }
        public string EventQueueName { get; set; }
        public DateTime EventGenerationTime { get; set; }
        public string Type { get; set; }
        public string SearchContextId { get; set; }
        public string EventOriginatorId { get; set; }
        public string EventOriginatorName { get; set; }
        public string EventDetail { get; set; }

        public Event()
        {

        }

        public Event(PlatformEvent ev)
        {
            Id = ev.Id;
            EventQueueName = ev.EventQueueName.ToString();
            EventGenerationTime = ev.EventGenerationTime.AddHours(-9);
            Type = ev.Type.ToString();
            SearchContextId = ev.SearchContextId;
            EventOriginatorId = ev.EventOriginatorId;
            EventOriginatorName = ev.EventOriginatorName;

            var eventDetail1 = ev.EventDetail as SingleDocumentEventDetail;
            if (eventDetail1 != null)
            {
                EventDetail = "Doc = " + eventDetail1;
            }

            var eventDetail2 = ev.EventDetail as UpdateDocumentEventDetail;
            if (eventDetail2 != null)
            {
                EventDetail = string.Join(" ,Att = ", eventDetail2.UpdatedAttributes.Select(x => $"k:{x.Key} v:{x.Value}").ToArray());
            }

            var eventDetail = ev.EventDetail as MailItemFiledEventDetail;
            if (eventDetail != null)
            {
                EventDetail = string.Join("Doc = ", eventDetail.Documents.Select(x => x.Id).ToArray());
            }


        }
    }

}
