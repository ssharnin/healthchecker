﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer
{
    public class Folder
    {
        public string Id { get; set; }
        public string MetadataId { get; set; }
        public string MirrorDate { get; set; }
    }
}
